# code test place
learn and implement some interesting code.<br>
### A lot of Polygons
![](images/polygons.gif)
### FSM
use FSM(finite state machine) to control charactor animations.<br>
![](images/unityyoko.gif)
### Entity-Component-System
Learned from [tutorial](https://www.youtube.com/channel/UC1XihgHdkNOQd5IBHnIZWbA "youtube channal") made by [Vittorio Romeo](https://github.com/SuperV1234 "github")<br>
write the game script like unity c# by using c++
```cpp
auto& player(m_EntityManager->AddEntity());
player.AddComponent<TransformComponent>();
player.AddComponent<SpriteComponent>(m_DirectX21.get(), m_TextureManager.get(), textureName, textureSize);
m_transform = &entity->GetComponent<TransformComponent>();
```
### 2D physics
learned from the [tutorial](https://gamedevelopment.tutsplus.com/tutorials/how-to-create-a-custom-2d-physics-engine-the-basics-and-impulse-resolution--gamedev-6331)<br>
implemented the simple 2D impulse physics.<br>
![](images/physics.gif)