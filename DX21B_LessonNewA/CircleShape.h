#pragma once
#include "Shape.h"

class CircleShape : public Shape
{
public:
	CircleShape(float32 r);
	Shape* Clone() const override;
	void Initialize() override;
	void ComputeMass(float32 density) override;
	void SetOrient(float32 radians) override;
	void Draw(D3DX21* device) const override;
	Type GetType() const override;
};