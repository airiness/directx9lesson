#include "D3DX21.h"

#define FVF_VERTEX2D (D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1)

D3DX21::D3DX21()
{
}

D3DX21::~D3DX21()
{
}

bool D3DX21::Initialize(int screenWidth,int screenHeight,HWND hwnd)
{
	m_pD3D = Direct3DCreate9(D3D_SDK_VERSION);
	if (!m_pD3D)
	{
		MessageBox(nullptr, "DirectX Initial failed", "error", MB_OK | MB_ICONERROR);
		return false;
	}

	D3DPRESENT_PARAMETERS d3dpp = {};
	d3dpp.BackBufferWidth = screenWidth;
	d3dpp.BackBufferHeight = screenHeight;
	d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
	d3dpp.BackBufferCount = 1;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.Windowed = TRUE;
	d3dpp.EnableAutoDepthStencil = TRUE;
	d3dpp.AutoDepthStencilFormat = D3DFMT_D16;
	d3dpp.FullScreen_RefreshRateInHz = D3DPRESENT_RATE_DEFAULT;
	d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;// IMMEDIATE

	HRESULT hr = m_pD3D->CreateDevice(
		D3DADAPTER_DEFAULT,
		D3DDEVTYPE_HAL,
		hwnd,
		D3DCREATE_HARDWARE_VERTEXPROCESSING,
		&d3dpp,
		&m_pDevice
	);

	if (FAILED(hr))
	{
		MessageBox(nullptr, "DirectX Cteate failed", "error", MB_OK | MB_ICONERROR);
		return false;
	}

	hr = D3DXCreateLine(m_pDevice, &m_pLine);

	if (FAILED(hr))
	{
		MessageBox(nullptr, "DirectXLine Cteate failed", "error", MB_OK | MB_ICONERROR);
		return false;
	}



	//--set directx
	m_pDevice->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pDevice->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
	m_pDevice->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	m_pDevice->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	m_pDevice->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	m_pDevice->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_MIRROR);
	m_pDevice->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_MIRROR);
	m_pDevice->SetTextureStageState(0, D3DTSS_ALPHAOP, D3DTOP_MODULATE);

	return true;
}

void D3DX21::Shutdown()
{
	if (m_pLine)
	{
		m_pLine->Release();
		m_pLine = nullptr;
	}
	if (m_pDevice)
	{
		m_pDevice->Release();
		m_pDevice = nullptr;
	}
}

void D3DX21::BeginScene(int red, int green, int blue, int alpha)
{
	m_pDevice->Clear(0, nullptr, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER,
		D3DCOLOR_RGBA(red, green, blue, alpha), 1.0f, 0);
	m_pDevice->BeginScene();
	m_pDevice->SetFVF(FVF_VERTEX2D);
}

void D3DX21::EndScene()
{
	m_pDevice->EndScene();
	m_pDevice->Present(nullptr, nullptr, nullptr, nullptr);
}

LPDIRECT3DDEVICE9 D3DX21::GetDevice() const 
{
	return m_pDevice;
}

LPD3DXLINE D3DX21::GetLineDevive() const
{
	return m_pLine;
}
