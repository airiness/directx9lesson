#include <iostream>
#include "SpriteComponent.h"

SpriteComponent::SpriteComponent(D3DX21* device, TextureManager* textureManager, std::string textureName, Vec2 size)
	:m_device(device),m_TextureManager(textureManager), m_sTextureName(textureName),
	m_bIsAnimated(false), m_pAnimations(nullptr), m_size(size),m_origin(size / 2.0f)
{

}

void SpriteComponent::Init()
{
	m_transform = &entity->GetComponent<TransformComponent>();
}

void SpriteComponent::Update(const float & deltaTime)
{

}

void SpriteComponent::Draw()
{
}

void SpriteComponent::DrawSprite()
{
	float u0 = 0.0f, v0 = 0.0f;
	float u1 = 1.0f, v1 = 1.0f;
	if (m_bIsAnimated && m_frame!=0)
	{
		u0 = m_frame.x / m_pAnimations->cutx;
		v0 = m_frame.y / m_pAnimations->cuty;

		u1 = u0 + 1.0f / m_pAnimations->cutx;
		v1 = v0 + 1.0f / m_pAnimations->cuty;
		if (m_bHorizontalMirrorFlag)
		{
			std::swap(u0, u1);
		}
		if (m_bVerticalMirrorFlag)
		{
			std::swap(v0, v1);
		}
	}

	VERTEX2DUV v[] =
	{
		Vec4(-m_origin.x,			-m_origin.y,			0.0f, 1.0f),D3DCOLOR_RGBA(255, 255, 255, 255), Vec2(u0, v0),
		Vec4(m_size.x - m_origin.x,	-m_origin.y,			0.0f, 1.0f),D3DCOLOR_RGBA(255, 255, 255, 255), Vec2(u1, v0),
		Vec4(-m_origin.x,			m_size.y - m_origin.y,	0.0f, 1.0f),D3DCOLOR_RGBA(255, 255, 255, 255), Vec2(u0, v1),
		Vec4(m_size.x - m_origin.x,	m_size.y - m_origin.y,	0.0f, 1.0f),D3DCOLOR_RGBA(255, 255, 255, 255), Vec2(u1, v1),
	};

	D3DXMATRIX mtxS, mtxR, mtxT, mtxW;
	D3DXMatrixScaling(&mtxS, m_transform->GetScale().x, m_transform->GetScale().y, 1.0f);
	D3DXMatrixRotationZ(&mtxR, D3DXToRadian(m_transform->GetRotation()));
	D3DXMatrixTranslation(&mtxT, m_transform->GetPosition().x, m_transform->GetPosition().y, 0.0f);

	mtxW = mtxS * mtxR * mtxT;
	for (size_t i = 0; i < 4; i++)
	{
		D3DXVec4Transform(&v[i].position, &v[i].position, &mtxW);
	}

	LPDIRECT3DTEXTURE9 texturepointer = nullptr;
	auto ret = m_TextureManager->GetTexture(m_sTextureName, texturepointer);
	assert(ret);
	m_device->GetDevice()->SetTexture(0, texturepointer);
	m_device->GetDevice()->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, v, sizeof(VERTEX2DUV));

}

SpriteComponent::AnimationsInfo::AnimationsInfo(size_t cx, size_t cy, std::initializer_list<Animation> l)
	:cutx(cx), cuty(cy)
{
	auto iter = l.begin();
	for (auto & a : l)
	{
		animations.push_back(std::move(a));
	}
}

void SpriteComponent::SetAnimation(size_t cx, size_t cy, std::initializer_list<Animation> l)
{
	m_bIsAnimated = true;
	m_pAnimations = std::make_unique<AnimationsInfo>(cx, cy, l);
}

void SpriteComponent::SetOrigin(Vec2 & origin)
{
	m_origin.x = m_size.x * origin.x;
	m_origin.y = m_size.y * origin.y;
}

SpriteComponent::AnimationsInfo & SpriteComponent::GetAnimations()
{
	return *m_pAnimations;
}

Vec2 & SpriteComponent::GetFrame()
{
	return m_frame;
}

bool SpriteComponent::SetAnimationFrame(const float & deltaTime, int state, bool isLoop, bool isStatLast)
{
	bool ret = false;
	static int statebefore = -10;

	int animNum = 0;

	if (state - 1000 >= 0)
	{
		animNum = state - 1000;
		m_bHorizontalMirrorFlag = true;
	}
	else
	{
		animNum = state;
		m_bHorizontalMirrorFlag = false;
	}

	static float cdtime = 0.0f;
	static float timePerFrame = 0.0f;
	if (statebefore!=state)
	{
		statebefore = state;
		m_frame = m_pAnimations->animations[animNum].begin;
		cdtime = 0.0f; 
		timePerFrame = m_pAnimations->animations[animNum].elapseTime /
			(m_pAnimations->animations[animNum].end.x - m_pAnimations->animations[animNum].begin.x);
	}
	else
	{
		cdtime += deltaTime;
		if (cdtime >= timePerFrame)
		{
			if (m_frame.x < m_pAnimations->animations[animNum].end.x)
			{
				m_frame.x++;
			}
			else
			{
				if (isLoop)
				{
					if (isStatLast)
					{

					}
					else
					{
						m_frame.x = m_pAnimations->animations[animNum].begin.x;
					}
				}
				else
				{
					ret = true;
				}
			}
			cdtime = 0.0f;
		}
	}

	return ret;
}

void SpriteComponent::SetHorizontalMirrorFlag(bool flag)
{
	m_bHorizontalMirrorFlag = flag;
}

void SpriteComponent::SetVerticalMirrorFlag(bool flag)
{
	m_bVerticalMirrorFlag = flag;
}

