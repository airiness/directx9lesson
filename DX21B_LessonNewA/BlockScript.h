#pragma once
#include "SpriteComponent.h"
#include "RigidBodyComponent.h"

class BulletScript : public Component
{
private:
	TransformComponent* m_transform;
	SpriteComponent* m_sprite;
	RigidBodyComponent* m_physics;
private:


public:
	BulletScript() = default;

	void Init() override;
	void Update(const float& deltaTime) override;
	void Draw() override;
};