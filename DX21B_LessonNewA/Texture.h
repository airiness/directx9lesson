/*2019.7
 *author:zhang ruisong
 *texture class
 */

#pragma once
#include "pch.h"
#include "D3DX21.h"

class TextureManager;
class Texture
{
	friend class TextureManager;
private:
	Texture(D3DX21*, const std::string&, int, int);
	void ReleaseTexture();
	bool LoadTextureFromFile(D3DX21*, const std::string&, int, int);
	bool ok();
public:
	Texture();
	Texture(Texture&&);
	Texture& operator=(Texture&&);
	Texture(const Texture&) = delete;
	Texture& operator=(const Texture&) = delete;
	~Texture();
private:
	int m_width;
	int m_height;
	LPDIRECT3DTEXTURE9 m_pTexture;

	bool m_isAnimated;
	std::vector<int>* m_pAnimationInfo;
};
