#include "RandGenerator.h"

RandGenerator* RandGenerator::pInstance = nullptr;

RandGenerator::RandGenerator()
{
}


RandGenerator::~RandGenerator()
{
	Shutdown();
}

RandGenerator * RandGenerator::GetSinglton()
{
	if (!pInstance)
	{
		Initialize();
	}
	return pInstance;
}

void RandGenerator::Initialize()
{
	if (!pInstance)
	{
		pInstance = new RandGenerator();
	}
}

void RandGenerator::Shutdown()
{
	if (pInstance)
	{
		delete pInstance;
		pInstance = nullptr;
	}
}

void RandGenerator::SetSeed(int seed)
{
	mt.seed(seed);
}

int RandGenerator::Randint(int from, int to)
{
	std::uniform_int_distribution<int> disti(from, to);
	return disti(mt);

}

float RandGenerator::Randreal(float from, float to)
{
	std::uniform_real_distribution<float> distr(from, to);
	return distr(mt);
}
