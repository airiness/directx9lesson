#include "DebugFont.h"

#define DEBUG_PRINTF_BUFFER_MAX (256)

DebugFont::DebugFont()
	:m_font(nullptr)
{
}

DebugFont::~DebugFont()
{
}

void DebugFont::Initialize(D3DX21* device)
{
#if defined(_DEBUG) || defined(DEBUG)

	D3DXCreateFont(device->GetDevice(),
		24, 0, 0, 0,
		FALSE,
		SHIFTJIS_CHARSET,
		OUT_DEFAULT_PRECIS,
		DEFAULT_QUALITY,
		DEFAULT_PITCH,
		"HGP�n�p�p�߯�ߑ�",
		&m_font);

#endif
}

void DebugFont::Shutdown()
{
#if defined(_DEBUG) || defined(DEBUG)
	if (m_font)
	{
		m_font->Release();
		m_font = nullptr;
	}
#endif // defined

}

void DebugFont::DebugDraw(int x, int y, int screenWidth,int screenHeight, const char * pFormat, ...)
{
#if defined(_DEBUG)||(DEBUG)
	RECT rect = { x, y, screenWidth, screenHeight};

	va_list argp;
	va_start(argp, pFormat);
	// ������p�o�b�t�@
	char buf[DEBUG_PRINTF_BUFFER_MAX];
	vsprintf_s(buf, DEBUG_PRINTF_BUFFER_MAX, pFormat, argp);
	va_end(argp);

	m_font->DrawText(NULL,
		buf,
		-1, &rect, DT_LEFT,
		D3DCOLOR_RGBA(255, 255, 255, 255));

#else
	UNREFERENCED_PARAMETER(pFormat);
	UNREFERENCED_PARAMETER(x);
	UNREFERENCED_PARAMETER(y);
#endif
}
