#include "Timer.h"



Timer::Timer()
	:m_TimerStopped(true),
	m_TicksPerSec(0),
	m_StopTime(0),
	m_LastElapsedTime(0),
	m_BaseTime(0)
{
}


Timer::~Timer()
{
}

void Timer::Initialize()
{
	LARGE_INTEGER ticksPerSec = { 0 };
	QueryPerformanceFrequency(&ticksPerSec);
	m_TicksPerSec = ticksPerSec.QuadPart;
}

void Timer::Reset()
{
	LARGE_INTEGER time = GetAdjustedCurrentTime();

	m_BaseTime = m_LastElapsedTime = time.QuadPart;
	m_StopTime = 0;
	m_TimerStopped = false;
}

void Timer::Start()
{
	// 現在の時間を取得
	LARGE_INTEGER time = { 0 };
	QueryPerformanceCounter(&time);

	// 今まで計測がストップしていたら
	if (m_TimerStopped) {
		// 止まっていた時間を差し引いて基本時間を更新
		m_BaseTime += time.QuadPart - m_StopTime;
	}

	m_StopTime = 0;
	m_LastElapsedTime = time.QuadPart;
	m_TimerStopped = false;
}

void Timer::Stop()
{
	if (m_TimerStopped) return;

	LARGE_INTEGER time = { 0 };
	QueryPerformanceCounter(&time);

	m_LastElapsedTime = m_StopTime = time.QuadPart; // 停止時間を記録
	m_TimerStopped = true;
}

void Timer::Advance()
{
	m_StopTime += m_TicksPerSec / 10;
}

double Timer::GetTime()
{
	LARGE_INTEGER time = GetAdjustedCurrentTime();

	return double(time.QuadPart - m_BaseTime) / double(m_TicksPerSec);
}

double Timer::GetAbsoluteTime()
{
	LARGE_INTEGER time = { 0 };
	QueryPerformanceCounter(&time);

	return time.QuadPart / double(m_TicksPerSec);
}

float Timer::GetElapsedTIme()
{
	LARGE_INTEGER time = GetAdjustedCurrentTime();

	double elapsed_time = (float)((double)(time.QuadPart - m_LastElapsedTime) / (double)m_TicksPerSec);
	m_LastElapsedTime = time.QuadPart;

	// タイマーが正確であることを保証するために、更新時間を０にクランプする。
	// elapsed_timeは、プロセッサが節電モードに入るか、何らかの形で別のプロセッサにシャッフルされると、この範囲外になる可能性がある。
	// よって、メインスレッドはSetThreadAffinityMaskを呼び出して、別のプロセッサにシャッフルされないようにする必要がある。
	// 他のワーカースレッドはSetThreadAffinityMaskを呼び出すべきではなく、メインスレッドから収集されたタイマーデータの共有コピーを使用すること。
	if (elapsed_time < 0.0f) {
		elapsed_time = 0.0f;
	}

	return (float)elapsed_time;
}

bool Timer::IsStoped()
{
	return m_TimerStopped;
}

void Timer::LimitThreadAffinityToCurrentProc()
{
	HANDLE hCurrentProcess = GetCurrentProcess();

	// Get the processor affinity mask for this process
	DWORD_PTR dwProcessAffinityMask = 0;
	DWORD_PTR dwSystemAffinityMask = 0;

	if (GetProcessAffinityMask(hCurrentProcess, &dwProcessAffinityMask, &dwSystemAffinityMask) != 0 && dwProcessAffinityMask) {
		// Find the lowest processor that our process is allows to run against
		DWORD_PTR dwAffinityMask = (dwProcessAffinityMask & ((~dwProcessAffinityMask) + 1));

		// Set this as the processor that our thread must always run against
		// This must be a subset of the process affinity mask
		HANDLE hCurrentThread = GetCurrentThread();
		if (INVALID_HANDLE_VALUE != hCurrentThread) {
			SetThreadAffinityMask(hCurrentThread, dwAffinityMask);
			CloseHandle(hCurrentThread);
		}
	}

	CloseHandle(hCurrentProcess);
}

LARGE_INTEGER Timer::GetAdjustedCurrentTime()
{
	LARGE_INTEGER time;
	if (m_StopTime != 0) {
		time.QuadPart = m_StopTime;
	}
	else {
		QueryPerformanceCounter(&time);
	}

	return time;
}
