#pragma once
#include "PIMath.h"

struct VERTEX2D
{
	Vec4 position;
	Color color;
};

struct VERTEX2DUV
{
	Vec4 position;
	Color color;
	Vec2 uv;
};

