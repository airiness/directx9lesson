#pragma once
#include "Object.h"
class Bullet : public Object
{
public:
	Bullet();
	~Bullet();

	virtual void Initilize(TextureManager* tm, const std::string& texName, 
		const D3DXVECTOR2& pos, const D3DXVECTOR2& size) override;
	virtual void Update(Input* input, float deltaTime) override;
	virtual void Draw(D3DX21 * device, TextureManager* tm) override;

private:

};

