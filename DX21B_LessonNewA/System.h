#pragma once
#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif
#include "TextureManager.h"
#include "DebugFont.h"
#include "Timer.h"
#include "Input.h"
#include "PhysicsWorld.h"
#include "EntityComponentSys.h"

class System
{
public:
	System();
	System(const System&) = delete;
	~System();

	bool Initialize();
	void Shutdown();
	void Run();

	LRESULT CALLBACK MessageHandler(HWND, UINT, WPARAM, LPARAM);
private:
	void InitializeObjects();
	void UpdateObjects(float);
	void DrawObjects();

	void Update();
	void Draw();
	void InitializeWindows(int&, int&);
	void ShutdownWindows();

private:
	LPCSTR m_applicationClassName;
	LPCSTR m_applicationName;
	HINSTANCE m_hinstance;
	HWND m_hwd;

	int m_screenWidth;
	int m_screenHeight;

	std::unique_ptr<DebugFont> m_DebugFont;
	std::unique_ptr<Timer> m_Timer;
	std::unique_ptr<D3DX21> m_DirectX21;
	std::unique_ptr<Input> m_Input;
	std::unique_ptr<PhysicsWorld> m_PhysicsWorld;
	std::unique_ptr<EntityManager> m_EntityManager;
	std::unique_ptr<TextureManager> m_TextureManager;

private:
	int m_FrameCount = 0;
	int m_FPSBaseFrameCount = 0;
	double m_FPSBaseTime = 0.0;
	float m_FPS = 0;
	double m_deltaTime = 0.0;
};

static LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

static System* ApplicationHandle = nullptr;