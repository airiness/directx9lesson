#pragma once
#include "PhysicsHead.h"

const float32 gravityScale = 50.0f;
const Vec2 gravity(0, 10.0f * gravityScale);

class PhysicsWorld
{
public:
	PhysicsWorld();
	~PhysicsWorld();

	void Initialize(float32 dt,uint32 iter);
	void Step();
	void DrawShape(D3DX21* device);
	Body* CreateBody(Shape* shape, Vec2& position);
	void SetGravity(float gravity);
	void DestoryBody(Body* body);


private:
	float32 m_dt;

	int32 m_bodyCount;
	uint32 m_iterations;

	std::vector<Body*> m_bodies;
	std::vector<Manifold> m_contacts;

};

