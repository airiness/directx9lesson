#pragma once
#include "pch.h"
#include "EntityComponentSys.h"

class TransformComponent : public Component
{
private:
	Vec2 m_position;
	Vec2 m_scale;
	float m_rotation;

public:
	TransformComponent();

	void Init() override;

	Vec2& GetPosition();
	Vec2& GetScale();
	float GetRotation();

	void SetPosition(const Vec2& position);
};