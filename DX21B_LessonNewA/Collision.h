#pragma once
#include "Shape.h"
#include "CircleShape.h"
#include "PolygonShape.h"

class Manifold;
class Body;

extern std::function<void(Manifold*, Body*, Body*)> Dispatch[Shape::e_typeCount][Shape::e_typeCount];

void CircletoCircle(Manifold *m, Body *a, Body *b);
void CircletoPolygon(Manifold *m, Body *a, Body *b);
void PolygontoCircle(Manifold *m, Body *a, Body *b);
void PolygontoPolygon(Manifold *m, Body *a, Body *b);

