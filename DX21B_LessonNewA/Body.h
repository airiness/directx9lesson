#pragma once
#include "pch.h"

class Shape;

class Body
{
public:
	Body(Shape* shape, Vec2& pos);
	~Body();

	void ApplyForce(const Vec2& f);
	void ApplyImpulse(const Vec2& impulse, const Vec2& contactVector);
	void SetStatic();
	void SetOrient(float32 radians);

	Vec2& m_position;
	Vec2 m_velocity;

	float32 m_angularVelocity;
	float32 m_torque;
	float32 m_orient;

	Vec2 m_force;

	float32 m_inertia;	//inertia
	float32 m_iinertia;	//inverse inertia
	float32 m_mass;		//mass
	float32 m_imass;		//inverse mass

	float32 m_staticFriction;
	float32 m_dynamicFriction;
	float32 m_restitution;

	Shape* m_shape;

	Color m_color;
};

