#include "Texture.h"

Texture::Texture()
{
}

Texture::Texture(Texture &&rhs)
	:m_width(std::move(rhs.m_width)),
	m_height(std::move(rhs.m_height)),
	m_pTexture(std::move(rhs.m_pTexture)),
	m_isAnimated(std::move(rhs.m_isAnimated)),
	m_pAnimationInfo(std::move(rhs.m_pAnimationInfo))
{
}

Texture & Texture::operator=(Texture &&rhs)
{
	using std::swap;
	swap(this->m_width, rhs.m_width);
	swap(this->m_height, rhs.m_height);
	swap(this->m_pTexture, rhs.m_pTexture);
	swap(this->m_isAnimated, rhs.m_isAnimated);
	swap(this->m_pAnimationInfo, rhs.m_pAnimationInfo);
	return *this;
}

Texture::Texture(D3DX21 * device, const std::string& filename, int texWidth, int texHeight)
	:m_pTexture(nullptr), m_width(0), m_height(0), m_isAnimated(false), m_pAnimationInfo(nullptr)
{
	LoadTextureFromFile(device, filename, texWidth, texHeight);
}

void Texture::ReleaseTexture()
{
	if (m_pTexture)
	{
		m_pTexture->Release();
		m_pTexture = nullptr;
	}
}

bool Texture::LoadTextureFromFile(D3DX21 * device, const std::string& filename, int texWidth, int texHeight)
{

	HRESULT hr;
	hr = D3DXCreateTextureFromFileA(device->GetDevice(), filename.c_str(), &m_pTexture);
	if (SUCCEEDED(hr))
	{
		m_width = texWidth;
		m_height = texHeight;
		return true;
	}
	return false;
}

bool Texture::ok()
{
	if (m_pTexture)
	{
		return true;
	}
	return false;
}

Texture::~Texture()
{
	//a mistook here ,release work should be done in texture manager
	//ReleaseTexture();
}
