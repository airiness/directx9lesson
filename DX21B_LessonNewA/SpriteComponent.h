#include "TextureManager.h"
#include "TransformComponent.h"

class SpriteComponent : public Component
{
private:
	struct Animation
	{
		Vec2 begin;
		Vec2 end;
		float elapseTime;
	};

	struct AnimationsInfo
	{
		AnimationsInfo() = default;
		AnimationsInfo(size_t cx, size_t cy,std::initializer_list<Animation> l);
		size_t cutx;
		size_t cuty;
		std::vector<Animation> animations;
	};
private:
	TransformComponent* m_transform;

	std::string m_sTextureName;
	bool m_bIsAnimated;
	std::unique_ptr<AnimationsInfo> m_pAnimations;
	Vec2 m_frame = { 0,0 };

	Vec2 m_size;
	Vec2 m_origin;

	bool m_bHorizontalMirrorFlag = false;
	bool m_bVerticalMirrorFlag = false;

	D3DX21* m_device;
	TextureManager* m_TextureManager;
public:
	SpriteComponent(D3DX21* device, TextureManager* textureManager, std::string textureName,Vec2 size);

	void Init() override;
	void Update(const float& deltaTime) override;
	void Draw() override;

	void DrawSprite();

	void SetAnimation(size_t cx, size_t cy, std::initializer_list<Animation> l);
	void SetOrigin(Vec2& origin);
	
	AnimationsInfo& GetAnimations();
	Vec2& GetFrame();

	bool SetAnimationFrame(const float& deltaTime, int state, bool isLoop, bool isStayLast);

	void SetHorizontalMirrorFlag(bool flag);
	void SetVerticalMirrorFlag(bool flag);
};