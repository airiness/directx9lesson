#pragma once
#include <random>

#define Randi(f,t)  RandGenerator::GetSinglton()->Randint(f, t)
#define Randr(f,t)  RandGenerator::GetSinglton()->Randreal(f, t)

class RandGenerator
{
public:
	RandGenerator();
	~RandGenerator();

	static RandGenerator* GetSinglton();

	static void Initialize();
	static void Shutdown();

	void SetSeed(int seed);

	int Randint(int from, int to);
	float Randreal(float from, float to);

private:
	static RandGenerator* pInstance;
	std::mt19937 mt;
};

