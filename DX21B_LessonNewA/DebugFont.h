#pragma once
#include "D3DX21.h"
class DebugFont
{
public:
	DebugFont();
	~DebugFont();
public:
	//static DebugFont* Instance();


	void Initialize(D3DX21* device);
	void Shutdown();
	void DebugDraw(int x, int y,int screenWidth,int screenHeight, const char* pFormat, ...);
private:
	//static DebugFont* sInstance;

	LPD3DXFONT m_font;
};

