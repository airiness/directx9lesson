#include "System.h"

#include "PlayerScript.h"

System::System()
{
}


System::~System()
{
}

bool System::Initialize()
{
	bool result;

	InitializeWindows(m_screenWidth, m_screenHeight);
	m_DirectX21 = std::make_unique<D3DX21>();
	result = m_DirectX21->Initialize(m_screenWidth, m_screenHeight, m_hwd);
	if (!result)
	{
		return false;
	}

	m_Input = std::make_unique<Input>();
	result = m_Input->Initialize(m_hinstance, m_hwd, m_screenWidth, m_screenHeight);
	if (!result)
	{
		return false;
	}
	m_DebugFont = std::make_unique<DebugFont>();
	m_DebugFont->Initialize(m_DirectX21.get());

	m_Timer = std::make_unique<Timer>();
	m_Timer->Initialize();
	m_Timer->Start();

	RandGenerator::Initialize();
	RandGenerator::GetSinglton()->SetSeed(static_cast<int>(m_Timer->GetAbsoluteTime()));

	m_PhysicsWorld = std::make_unique<PhysicsWorld>();
	m_PhysicsWorld->Initialize(dt, 10);

	m_EntityManager = std::make_unique<EntityManager>();

	m_TextureManager = std::make_unique<TextureManager>(m_DirectX21.get());
	m_TextureManager->Initialize();

	InitializeObjects();

	return true;
}

void System::Shutdown()
{

	m_TextureManager->Shutdown();
	m_TextureManager.reset();
	m_Input->Shutdown();
	m_Input.reset();
	m_DebugFont->Shutdown();
	m_DebugFont.reset();
	m_EntityManager.reset();
	m_Timer.reset();
	m_PhysicsWorld.reset();
	m_DirectX21->Shutdown();
	m_DirectX21.reset();
	ShutdownWindows();
}

void System::Run()
{
	MSG msg;
	ZeroMemory(&msg, sizeof(MSG));
	
	while (msg.message != WM_QUIT)
	{

		if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			double time = m_Timer->GetTime();
			static double staticFrameTime = 0.0;
			if (time - staticFrameTime < dt)
			{

			}
			else
			{
				staticFrameTime = time;
				Update();
				Draw();
			}
		}
		if (m_Input->IsPress(DIK_ESCAPE))
		{
			return;
		}
	}
}


LRESULT CALLBACK System::MessageHandler(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

void System::InitializeObjects()
{
	auto& player(m_EntityManager->AddEntity());
	player.AddComponent<TransformComponent>();
	player.AddComponent<SpriteComponent>(m_DirectX21.get(), m_TextureManager.get(), "toko1024new.png", Vec2{ 128.f,128.f });
	player.AddComponent<RigidBodyComponent>(m_PhysicsWorld.get());
	player.AddComponent<PlayerScript>(m_Input.get());
}

void System::UpdateObjects(float deltaTime)
{
	m_EntityManager->Update(deltaTime);
	m_PhysicsWorld->Step();
}

void System::DrawObjects()
{
	m_EntityManager->Draw();
	m_PhysicsWorld->DrawShape(m_DirectX21.get());
}

void System::Update()
{
	static const auto  FPS_MEASUREMENT_TIME = 0.05;
	static int frameCount = 0;
	static int FPSBaseFrameCount = 0;
	static double FPSBaseTime = 0.0;
	static double deltatimeBaseTime = m_Timer->GetTime();
	double time = m_Timer->GetTime();
	if ((time - FPSBaseTime) >= FPS_MEASUREMENT_TIME)
	{
		m_FPS = float((frameCount - FPSBaseFrameCount) / (time - FPSBaseTime));
		FPSBaseTime = time;
		FPSBaseFrameCount = frameCount;
	}
	m_deltaTime = time - deltatimeBaseTime;
	deltatimeBaseTime = time;
	frameCount++;

	m_Input->Update();

	UpdateObjects(m_deltaTime);

}

void System::Draw()
{
	m_DirectX21->BeginScene(0, 0, 0, 255);

	DrawObjects();
	m_DebugFont->DebugDraw(32, 32, m_screenWidth, m_screenHeight, "fps:%.2f,deltaTime:%.6f", m_FPS,m_deltaTime);

	m_DirectX21->EndScene();
}

void System::InitializeWindows(int& screenWidth, int& screenHeight)
{

	auto windowStyle = WS_OVERLAPPEDWINDOW ^ WS_THICKFRAME ^ WS_MAXIMIZEBOX;

	ApplicationHandle = this;
	m_hinstance = GetModuleHandle(nullptr);

	m_applicationClassName = "Window";
	m_applicationName = "DX21";

	WNDCLASS wc = {};
	wc.lpfnWndProc = WndProc;
	wc.lpszClassName = m_applicationClassName;
	wc.hInstance = m_hinstance;
	wc.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)(COLOR_BACKGROUND + 1);
	
	RegisterClass(&wc);

	screenWidth = 1280;
	screenHeight = 720;

	RECT windowRect = { 0,0, screenWidth, screenHeight };
	AdjustWindowRect(&windowRect, windowStyle, FALSE);

	m_hwd = CreateWindow(
		m_applicationClassName,
		m_applicationName,
		windowStyle,
		0,0,
		windowRect.right - windowRect.left,
		windowRect.bottom - windowRect.top,
		nullptr,
		nullptr,
		m_hinstance,
		nullptr
	);

	ShowWindow(m_hwd, SW_SHOW);
	UpdateWindow(m_hwd);
}

void System::ShutdownWindows()
{
	DestroyWindow(m_hwd);
	m_hwd = nullptr;

	UnregisterClass(m_applicationName, m_hinstance);
	m_hinstance = nullptr;
	ApplicationHandle = nullptr;
}


LRESULT CALLBACK WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_CLOSE:
		if (MessageBox(hWnd, "本当に終了してよろしいですか？", "確認", MB_OKCANCEL | MB_DEFBUTTON2) == IDOK)
		{
			DestroyWindow(hWnd);
		}
		return 0;
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	};
	return ApplicationHandle->MessageHandler(hWnd, uMsg, wParam, lParam);
}
