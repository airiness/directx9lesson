#include "PhysicsHead.h"

std::function<void(Manifold*, Body*, Body*)> Dispatch[Shape::e_typeCount][Shape::e_typeCount] =
{
	{CircletoCircle, CircletoPolygon},
	{PolygontoCircle, PolygontoPolygon}
};



void CircletoCircle(Manifold * m, Body * a, Body * b)
{
	CircleShape* A = reinterpret_cast<CircleShape*>(a->m_shape);
	CircleShape* B = reinterpret_cast<CircleShape*>(b->m_shape);

	// Calculate translational vector, which is normal
	Vec2 normal = b->m_position - a->m_position;

	float32 dist_sqr = LenSq(&normal);
	float32 radius = A->m_radius + B->m_radius;

	// Not in contact
	if (dist_sqr >= radius * radius)
	{
		m->contact_count = 0;
		return;
	}

	float32 distance = std::sqrt(dist_sqr);

	m->contact_count = 1;

	if (distance == 0.0f)
	{
		m->penetration = A->m_radius;
		m->normal = Vec2(1, 0);
		m->contacts[0] = a->m_position;
	}
	else
	{
		m->penetration = radius - distance;
		m->normal = normal / distance; // Faster than using Normalized since we already performed sqrt
		m->contacts[0] = m->normal * A->m_radius + a->m_position;
	}
}

void CircletoPolygon(Manifold * m, Body * a, Body * b)
{
	CircleShape* A = reinterpret_cast<CircleShape*>(a->m_shape);
	PolygonShape* B = reinterpret_cast<PolygonShape*>(b->m_shape);

	m->contact_count = 0;

	// Transform circle center to Polygon model space
	Vec2 center = a->m_position;
	center = B->m_u.Transpose() * (center - b->m_position);

	// Find edge with minimum penetration
	// Exact concept as using support points in Polygon vs Polygon
	float32 separation = -FLT_MAX;
	uint32 faceNormal = 0;
	for (uint32 i = 0; i < B->m_vertexCount; ++i)
	{
		float32 s = Dot(B->m_normals[i], center - B->m_vertices[i]);

		if (s > A->m_radius)
			return;

		if (s > separation)
		{
			separation = s;
			faceNormal = i;
		}
	}

	// Grab face's vertices
	Vec2 v1 = B->m_vertices[faceNormal];
	uint32 i2 = faceNormal + 1 < B->m_vertexCount ? faceNormal + 1 : 0;
	Vec2 v2 = B->m_vertices[i2];

	// Check to see if center is within polygon
	if (separation < EPSILON)
	{
		m->contact_count = 1;
		m->normal = -(B->m_u * B->m_normals[faceNormal]);
		m->contacts[0] = m->normal * A->m_radius + a->m_position;
		m->penetration = A->m_radius;
		return;
	}

	// Determine which voronoi region of the edge center of circle lies within
	float32 dot1 = Dot(center - v1, v2 - v1);
	float32 dot2 = Dot(center - v2, v1 - v2);
	m->penetration = A->m_radius - separation;

	// Closest to v1
	if (dot1 <= 0.0f)
	{
		if (DistSqr(center, v1) > A->m_radius * A->m_radius)
			return;

		m->contact_count = 1;
		Vec2 n = v1 - center;
		n = B->m_u * n;
		Vec2Normalize(n);
		m->normal = n;
		v1 = B->m_u * v1 + b->m_position;
		m->contacts[0] = v1;
	}

	// Closest to v2
	else if (dot2 <= 0.0f)
	{
		if (DistSqr(center, v2) > A->m_radius * A->m_radius)
			return;

		m->contact_count = 1;
		Vec2 n = v2 - center;
		v2 = B->m_u * v2 + b->m_position;
		m->contacts[0] = v2;
		n = B->m_u * n;
		Vec2Normalize(n);
		m->normal = n;
	}

	// Closest to face
	else
	{
		Vec2 n = B->m_normals[faceNormal];
		if (Dot(center - v1, n) > A->m_radius)
			return;

		n = B->m_u * n;
		m->normal = -n;
		m->contacts[0] = m->normal * A->m_radius + a->m_position;
		m->contact_count = 1;
	}
}

void PolygontoCircle(Manifold * m, Body * a, Body * b)
{
	CircletoPolygon(m, b, a);
	m->normal = -m->normal;
}

float32 FindAxisLeastPenetration(uint32* faceIndex, PolygonShape* A, PolygonShape* B)
{
	float32 bestDistance = -FLT_MAX;
	uint32 bestIndex;

	for (uint32 i = 0; i < A->m_vertexCount; ++i)
	{
		// Retrieve a face normal from A
		Vec2 n = A->m_normals[i];
		Vec2 nw = A->m_u * n;

		// Transform face normal into B's model space
		Matrix22 buT = B->m_u.Transpose();
		n = buT * nw;

		// Retrieve support point from B along -n
		Vec2 s = B->GetSupport(-n);

		// Retrieve vertex on face from A, transform into
		// B's model space
		Vec2 v = A->m_vertices[i];
		v = A->m_u * v + A->m_body->m_position;
		v -= B->m_body->m_position;
		v = buT * v;

		// Compute penetration distance (in B's model space)
		float32 d = Dot(n, (s - v));

		// Store greatest distance
		if (d > bestDistance)
		{
			bestDistance = d;
			bestIndex = i;
		}
	}

	*faceIndex = bestIndex;
	return bestDistance;
}

void FindIncidentFace(Vec2* v, PolygonShape* RefPoly, PolygonShape* IncPoly, uint32 referenceIndex)
{
	Vec2 referenceNormal = RefPoly->m_normals[referenceIndex];

	// Calculate normal in incident's frame of reference
	referenceNormal = RefPoly->m_u * referenceNormal; // To world space
	referenceNormal = IncPoly->m_u.Transpose() * referenceNormal; // To incident's model space

	// Find most anti-normal face on incident polygon
	int32 incidentFace = 0;
	float32 minDot = FLT_MAX;
	for (uint32 i = 0; i < IncPoly->m_vertexCount; ++i)
	{
		float32 dot = Dot(referenceNormal, IncPoly->m_normals[i]);
		if (dot < minDot)
		{
			minDot = dot;
			incidentFace = i;
		}
	}

	// Assign face vertices for incidentFace
	v[0] = IncPoly->m_u * IncPoly->m_vertices[incidentFace] + IncPoly->m_body->m_position;
	incidentFace = incidentFace + 1 >= (int32)IncPoly->m_vertexCount ? 0 : incidentFace + 1;
	v[1] = IncPoly->m_u * IncPoly->m_vertices[incidentFace] + IncPoly->m_body->m_position;
}

int32 Clip(Vec2 n, float32 c, Vec2* face)
{
	uint32 sp = 0;
	Vec2 out[2] = {
	  face[0],
	  face[1]
	};

	// Retrieve distances from each endpoint to the line
	// d = ax + by - c
	float32 d1 = Dot(n, face[0]) - c;
	float32 d2 = Dot(n, face[1]) - c;

	// If negative (behind plane) clip
	if (d1 <= 0.0f) out[sp++] = face[0];
	if (d2 <= 0.0f) out[sp++] = face[1];

	// If the points are on different sides of the plane
	if (d1 * d2 < 0.0f) // less than to ignore -0.0f
	{
		// Push interesection point
		float32 alpha = d1 / (d1 - d2);
		out[sp] = face[0] + alpha * (face[1] - face[0]);
		++sp;
	}

	// Assign our new converted values
	face[0] = out[0];
	face[1] = out[1];

	assert(sp != 3);

	return sp;
}


void PolygontoPolygon(Manifold * m, Body * a, Body * b)
{
	PolygonShape* A = reinterpret_cast<PolygonShape*>(a->m_shape);
	PolygonShape* B = reinterpret_cast<PolygonShape*>(b->m_shape);
	m->contact_count = 0;

	// Check for a separating axis with A's face planes
	uint32 faceA;
	float32 penetrationA = FindAxisLeastPenetration(&faceA, A, B);
	if (penetrationA >= 0.0f)
		return;

	// Check for a separating axis with B's face planes
	uint32 faceB;
	float32 penetrationB = FindAxisLeastPenetration(&faceB, B, A);
	if (penetrationB >= 0.0f)
		return;

	uint32 referenceIndex;
	bool flip; // Always point from a to b

	PolygonShape* RefPoly; // Reference
	PolygonShape* IncPoly; // Incident

	// Determine which shape contains reference face
	if (BiasGreaterThan(penetrationA, penetrationB))
	{
		RefPoly = A;
		IncPoly = B;
		referenceIndex = faceA;
		flip = false;
	}

	else
	{
		RefPoly = B;
		IncPoly = A;
		referenceIndex = faceB;
		flip = true;
	}

	// World space incident face
	Vec2 incidentFace[2];
	FindIncidentFace(incidentFace, RefPoly, IncPoly, referenceIndex);

	//        y
	//        ^  ->n       ^
	//      +---c ------posPlane--
	//  x < | i |\
    //      +---+ c-----negPlane--
	//             \       v
	//              r
	//
	//  r : reference face
	//  i : incident poly
	//  c : clipped point
	//  n : incident normal

	// Setup reference face vertices
	Vec2 v1 = RefPoly->m_vertices[referenceIndex];
	referenceIndex = referenceIndex + 1 == RefPoly->m_vertexCount ? 0 : referenceIndex + 1;
	Vec2 v2 = RefPoly->m_vertices[referenceIndex];

	// Transform vertices to world space
	v1 = RefPoly->m_u * v1 + RefPoly->m_body->m_position;
	v2 = RefPoly->m_u * v2 + RefPoly->m_body->m_position;

	// Calculate reference face side normal in world space
	Vec2 sidePlaneNormal = (v2 - v1);
	Vec2Normalize(sidePlaneNormal);

	// Orthogonalize
	Vec2 refFaceNormal(sidePlaneNormal.y, -sidePlaneNormal.x);

	// ax + by = c
	// c is distance from origin
	float32 refC = Dot(refFaceNormal, v1);
	float32 negSide = -Dot(sidePlaneNormal, v1);
	float32 posSide = Dot(sidePlaneNormal, v2);

	// Clip incident face to reference face side planes
	if (Clip(-sidePlaneNormal, negSide, incidentFace) < 2)
		return; // Due to floating point error, possible to not have required points

	if (Clip(sidePlaneNormal, posSide, incidentFace) < 2)
		return; // Due to floating point error, possible to not have required points

	  // Flip
	m->normal = flip ? -refFaceNormal : refFaceNormal;

	// Keep points behind reference face
	uint32 cp = 0; // clipped points behind reference face
	float32 separation = Dot(refFaceNormal, incidentFace[0]) - refC;
	if (separation <= 0.0f)
	{
		m->contacts[cp] = incidentFace[0];
		m->penetration = -separation;
		++cp;
	}
	else
		m->penetration = 0;

	separation = Dot(refFaceNormal, incidentFace[1]) - refC;
	if (separation <= 0.0f)
	{
		m->contacts[cp] = incidentFace[1];

		m->penetration += -separation;
		++cp;

		// Average penetration
		m->penetration /= (float32)cp;
	}

	m->contact_count = cp;
}
