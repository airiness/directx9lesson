#include "TextureManager.h"
#include <fstream>
#include <utility>

TextureManager::TextureManager(D3DX21* device)
	:m_pDevice(device),m_pTextures(nullptr)
{
}

TextureManager::~TextureManager()
{
}

void TextureManager::Initialize()
{
	m_pTextures = std::make_unique<std::map<std::string, LPDIRECT3DTEXTURE9>>();
}

void TextureManager::Shutdown()
{
	m_pTextures.reset();
}

bool TextureManager::GetTexture(const std::string & texName, LPDIRECT3DTEXTURE9 & texture)
{
	bool result = true;
	if (m_pTextures->find(texName) == m_pTextures->end())
	{
		result = LoadTexturesFromFile(texName);
	}
	if (result)
	{
		texture = m_pTextures->at(texName);
	}

	return result;
}

void TextureManager::ClearAll()
{
	for (auto& t: *m_pTextures)
	{
		ReleaseTexture(&(t.second));
	}
	m_pTextures->clear();
}

void TextureManager::UnLoad(const std::string & texName)
{
	ReleaseTexture(&(m_pTextures->at(texName)));
	m_pTextures->erase(texName);
}

bool TextureManager::GetTextureSize(const std::string & texName, Vec2 & texSize)
{
	bool result;
	LPDIRECT3DTEXTURE9 texture;
	if (result = GetTexture(texName, texture))
	{
		D3DSURFACE_DESC sd;
		texture->GetLevelDesc(0, &sd);
		texSize.x = sd.Width;
		texSize.y = sd.Height;
	}

	return result;
}



void TextureManager::ReleaseTexture(LPDIRECT3DTEXTURE9 * texture)
{
	if (texture)
	{
		(*texture)->Release();
		(*texture) = nullptr;
	}
}

bool TextureManager::LoadTexturesFromFile(const std::string & filename)
{
	bool result = false;
	std::string route{ "Assets/Textures/" };
	LPDIRECT3DTEXTURE9 texture;
	auto hr = D3DXCreateTextureFromFileA(m_pDevice->GetDevice(), route.append(filename).c_str(), &texture);

	if (SUCCEEDED(hr))
	{
		m_pTextures->emplace(filename,std::move(texture));
		result = true;
	}
	else
	{
		result = false;
	}

	return result;
}
