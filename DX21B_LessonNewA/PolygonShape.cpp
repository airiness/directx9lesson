#include "PhysicsHead.h"

void PolygonShape::Initialize()
{
	ComputeMass(2.f);
}

Shape * PolygonShape::Clone() const
{
	auto poly = new PolygonShape();
	poly->m_u = m_u;
	for (uint32 i = 0; i < m_vertexCount; ++i)
	{
		poly->m_vertices[i] = m_vertices[i];
		poly->m_normals[i] = m_normals[i];
	}
	poly->m_vertexCount = m_vertexCount;
	return poly;
}

void PolygonShape::ComputeMass(float32 density)
{
	Vec2 c(0.f, 0.f);
	float32 area = 0.f;
	float32 I = 0.f;
	const float32 k_inv3 = 1.f / 3.f;

	for (uint32 i1 = 0; i1 < m_vertexCount; ++i1)
	{
		Vec2 p1(m_vertices[i1]);
		uint32 i2 = i1 + 1 < m_vertexCount ? i1 + 1 : 0;
		Vec2 p2(m_vertices[i2]);

		float32 D = Cross(p1, p2);
		float32 triangleArea = 0.5f * D;

		area += triangleArea;

		c += triangleArea * k_inv3 * (p1 + p2);

		float32 intx2 = p1.x * p1.x + p2.x * p1.x + p2.x * p2.x;
		float32 inty2 = p1.y * p1.y + p2.y * p1.y + p2.y * p2.y;
		I += (0.25f * k_inv3 * D) * (intx2 + inty2);
	}

	c *= 1.0f / area;

	for (uint32 i = 0; i < m_vertexCount; ++i)
		m_vertices[i] -= c;

	m_body->m_mass = density * area;
	m_body->m_imass = (m_body->m_mass) ? 1.0f / m_body->m_mass : 0.0f;
	m_body->m_inertia = I * density;
	m_body->m_iinertia = m_body->m_inertia ? 1.0f / m_body->m_inertia : 0.0f;
}

void PolygonShape::SetOrient(float32 radians)
{
	m_u.Set(radians);
}

void PolygonShape::Draw(D3DX21 * device) const
{
	if (m_shouldDraw)
	{
		//draw the polygon
		auto lineDev = device->GetLineDevive();

		Vec2 v[MaxPolyVertexCount];
		for (uint32 i = 0; i < m_vertexCount; ++i)
		{
			v[i] = m_body->m_position + m_u * m_vertices[i];
		}
		v[m_vertexCount] = v[0];

		lineDev->SetWidth(3.f);
		lineDev->SetAntialias(TRUE);
		lineDev->Draw(v,m_vertexCount+1, m_body->m_color);

		//draw orient
	}
}

Shape::Type PolygonShape::GetType() const
{
	return e_polygon;
}

void PolygonShape::SetBox(float32 width, float32 height)
{
	m_vertexCount = 4;
	m_vertices[0] = { -width ,-height };
	m_vertices[1] = { width ,-height };
	m_vertices[2] = { width ,height };
	m_vertices[3] = { -width ,height };
	m_normals[0] = { 0.f, -1.f };
	m_normals[1] = { 1.f, 0.f };
	m_normals[2] = { 0.f, 1.f };
	m_normals[3] = { -1.f, 0.f };
}

void PolygonShape::Set(Vec2 * vertices, uint32 count)
{
	// No hulls with less than 3 vertices (ensure actual polygon)
	assert(count > 2 && count <= MaxPolyVertexCount);
	count = (std::min)((int32)count, MaxPolyVertexCount);

	// Find the right most point on the hull
	int32 rightMost = 0;
	float32 highestXCoord = vertices[0].x;
	for (uint32 i = 1; i < count; ++i)
	{
		float32 x = vertices[i].x;
		if (x > highestXCoord)
		{
			highestXCoord = x;
			rightMost = i;
		}
		else if (x == highestXCoord) // If matching x then take farthest negative y
		{
			if (vertices[i].y < vertices[rightMost].y)
			{
				rightMost = i;
			}
		}
	}

	int32 hull[MaxPolyVertexCount];
	int32 outCount = 0;
	int32 indexHull = rightMost;

	for (;;)
	{
		hull[outCount] = indexHull;

		// Search for next index that wraps around the hull
		// by computing cross products to find the most counter-clockwise
		// vertex in the set, given the previos hull index
		int32 nextHullIndex = 0;
		for (int32 i = 1; i < (int32)count; ++i)
		{
			// Skip if same coordinate as we need three unique
			// points in the set to perform a cross product
			if (nextHullIndex == indexHull)
			{
				nextHullIndex = i;
				continue;
			}

			// Cross every set of three unique vertices
			// Record each counter clockwise third vertex and add
			// to the output hull
			// See : http://www.oocities.org/pcgpe/math2d.html
			Vec2 e1 = vertices[nextHullIndex] - vertices[hull[outCount]];
			Vec2 e2 = vertices[i] - vertices[hull[outCount]];
			float32 c = Cross(e1, e2);
			if (c < 0.0f)
				nextHullIndex = i;

			// Cross product is zero then e vectors are on same line
			// therefor want to record vertex farthest along that line
			if (c == 0.0f && LenSq(&e2) > LenSq(&e1))
				nextHullIndex = i;
		}

		++outCount;
		indexHull = nextHullIndex;

		// Conclude algorithm upon wrap-around
		if (nextHullIndex == rightMost)
		{
			m_vertexCount = outCount;
			break;
		}
	}

	// Copy vertices into shape's vertices
	for (uint32 i = 0; i < m_vertexCount; ++i)
		m_vertices[i] = vertices[hull[i]];

	// Compute face normals
	for (uint32 i1 = 0; i1 < m_vertexCount; ++i1)
	{
		uint32 i2 = i1 + 1 < m_vertexCount ? i1 + 1 : 0;
		Vec2 face = m_vertices[i2] - m_vertices[i1];

		// Ensure no zero-length edges, because that's bad
		assert(LenSq(&face) > EPSILON * EPSILON);

		// Calculate normal with 2D cross product between vector and scalar
		m_normals[i1] = Vec2(face.y, -face.x);
		Vec2Normalize(m_normals[i1]);
	}
}

Vec2 PolygonShape::GetSupport(const Vec2 & dir)
{
	float32 bestProjection = -FLT_MAX;
	Vec2 bestVertex;

	for (uint32 i = 0; i < m_vertexCount; ++i)
	{
		Vec2 v = m_vertices[i];
		float32 projection = Dot(v, dir);

		if (projection > bestProjection)
		{
			bestVertex = v;
			bestProjection = projection;
		}
	}

	return bestVertex;
}
