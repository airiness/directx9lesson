/*2019.7
 *author:zhang ruisong
 *class of timer,from yohe sennsei c style code
 */
#pragma once
#include "pch.h"
class Timer
{
public:
	Timer();
	~Timer();

	void Initialize();
	void Reset();
	void Start();
	void Stop();
	void Advance();
	double GetTime();
	double GetAbsoluteTime();
	float GetElapsedTIme();
	bool IsStoped();
	void LimitThreadAffinityToCurrentProc();


private:
	LARGE_INTEGER GetAdjustedCurrentTime();

	bool m_TimerStopped;
	LONGLONG m_TicksPerSec;
	LONGLONG m_StopTime;
	LONGLONG m_LastElapsedTime;
	LONGLONG m_BaseTime;
};

