#pragma once
#include "Input.h"
#include "SpriteComponent.h"
#include "RigidBodyComponent.h"

class BulletScript;

class PlayerScript : public Component
{
private:
	enum TokoState  
	{
		ATTACK = 0,
		ATTACK_LEFT = 1000,
		BLINK = 1,
		BLINK_LEFT = 1001,
		CROUCH = 2,
		CROUCH_LEFT = 1002,
		DAMAGE = 3,
		DAMAGE_LEFT = 1003,
		DAMAGE_DONW = 4,
		DAMAGE_DOWN_LEFT = 1004,
		DAMAGE_RETURN = 5,
		DAMAGE_RETURN_LEFT = 1005,
		IDLE = 6,
		IDLE_LEFT = 1006,
		JUMP = 7,
		JUMP_LEFT = 1007,
		LOOKAROUND = 8,
		LOOKAROUND_LEFT = 1008,
		NEGATIVE = 9,
		NEGATIVE_LEFT = 1009,
		POINTAT = 10,
		POINTAT_LEFT = 1010,
		POSITIVE = 11,
		POSITIVE_LEFT = 1011,
		PUNCH = 12,
		PUNCH_LEFT = 1012,
		RUN = 13,
		RUN_LEFT = 1013,
		TURN = 14,
		TURN_LEFT = 1014,
		WALK = 15,
		WALK_LEFT = 1015,
		WARMING_UP = 16,
		WARMING_UP_LEFT = 1016,
		WAVE_HAND = 17,
		WAVE_HAND_LEFT =1017
	};
private:
	TransformComponent* m_transform;
	SpriteComponent* m_sprite;
	RigidBodyComponent* m_physics;
	Input& m_input;

	float m_speedwalk;
	float m_speedrun;
	TokoState m_tokoState = TokoState::IDLE;
public:
	PlayerScript(Input* input);

	void Init() override;
	void Update(const float& deltaTime) override;
	void Draw() override;
};