#include <iostream>
#include "PlayerScript.h"

PlayerScript::PlayerScript(Input * input)
	: m_input(*input)
{
}

void PlayerScript::Init()
{
	m_transform = &entity->GetComponent<TransformComponent>();
	m_sprite = &entity->GetComponent<SpriteComponent>();
	m_physics = &entity->GetComponent<RigidBodyComponent>();

	m_transform->SetPosition(Vec2{ 600.f, 200.0f });
	m_sprite->SetAnimation(16, 16,
		{ 
			{{0,0},{12,0},  0.5},//attack
			{{0,1},{1,1},   0.1},//blink
			{{2, 1},{6,1},  0.3},//crouch
			{{7,1},{10,1},  0.4},//damage
			{{5,0},{5,2},   0.4},//damage down 
			{{6,2},{13,2},  0.4},//damage return 
			{{0,3},{3,3},   0.4},//idle
			{{4,3},{8,3},   0.4},//jump
			{{0,4},{8,4},   0.4},//look around
			{{9,4},{12,4},  0.4},//negative
			{{0,5},{5,5},   0.4},//point at
			{{6,5},{7,5},   0.4},//positive
			{{0,6},{9,6},   0.4},//punch
			{{0,7},{7,7},   0.4},//run
			{{0,8},{13,8},  0.4},//turn
			{{0,9},{5,9},   0.4},//walk
			{{0,10},{13,10},0.4},//warming up
			{{0,11},{3,11}, 0.4},//wave hand
		});
	m_tokoState = TokoState::IDLE;
	m_speedwalk = 10.0f;
	m_speedrun = m_speedwalk * 1.8f;
}

void PlayerScript::Update(const float & deltaTime)
{
	static bool animFinished = false;
	static bool animLoopFlag = true;
	static bool animStayLast = false;

	switch (m_tokoState)
	{
	case PlayerScript::ATTACK:
		animLoopFlag = false;
		if (animFinished)
		{
			animFinished = false;
			m_tokoState = TokoState::IDLE;
		}
		break;
	case PlayerScript::ATTACK_LEFT:
		animLoopFlag = false;
		if (animFinished)
		{
			animFinished = false;
			m_tokoState = TokoState::IDLE_LEFT;
		}
		break;
	case PlayerScript::BLINK:
		break;
	case PlayerScript::BLINK_LEFT:
		break;
	case PlayerScript::CROUCH:
		animLoopFlag = true;
		animStayLast = true;
		if (!m_input.IsPress(DIK_S))
		{
			animStayLast = false;
			m_tokoState = TokoState::IDLE;
		}
		break;
	case PlayerScript::CROUCH_LEFT:
		animLoopFlag = true;
		animStayLast = true;
		if (!m_input.IsPress(DIK_S))
		{
			animStayLast = false;
			m_tokoState = TokoState::IDLE_LEFT;
		}
		break;
	case PlayerScript::DAMAGE:
		break;
	case PlayerScript::DAMAGE_LEFT:
		break;
	case PlayerScript::DAMAGE_DONW:
		animLoopFlag = false;
		if (animFinished)
		{
			animFinished = false;
			m_tokoState = TokoState::DAMAGE_RETURN;
		}
		break;
	case PlayerScript::DAMAGE_DOWN_LEFT:
		animLoopFlag = false;
		if (animFinished)
		{
			animFinished = false;
			m_tokoState = TokoState::DAMAGE_RETURN_LEFT;
		}
		break;
	case PlayerScript::DAMAGE_RETURN:
		animLoopFlag = false;
		if (animFinished)
		{
			animFinished = false;
			m_tokoState = TokoState::IDLE;
		}
		break;
	case PlayerScript::DAMAGE_RETURN_LEFT:
		animLoopFlag = false;
		if (animFinished)
		{
			animFinished = false;
			m_tokoState = TokoState::IDLE_LEFT;
		}
		break;
	case PlayerScript::IDLE:
		animLoopFlag = true;
		if (m_input.IsPress(DIK_D))
		{
			m_tokoState = TokoState::WALK;
		}
		if (m_input.IsPress(DIK_A))
		{
			m_tokoState = TokoState::WALK_LEFT;
		}
		if (m_input.IsTrigger(DIK_J))
		{
			m_tokoState = TokoState::ATTACK;
		}
		if (m_input.IsTrigger(DIK_K))
		{
			m_tokoState = TokoState::PUNCH;
		}
		if (m_input.IsPress(DIK_S))
		{
			m_tokoState = TokoState::CROUCH;
		}
		if (m_input.IsPress(DIK_SPACE))
		{
			m_tokoState = TokoState::JUMP;
		}
		
		break;
	case PlayerScript::IDLE_LEFT:
		animLoopFlag = true;
		if (m_input.IsPress(DIK_D))
		{
			m_tokoState = TokoState::WALK;
		}
		if (m_input.IsPress(DIK_A))
		{
			m_tokoState = TokoState::WALK_LEFT;
		}
		if (m_input.IsTrigger(DIK_J))
		{
			m_tokoState = TokoState::ATTACK_LEFT;
		}
		if (m_input.IsTrigger(DIK_K))
		{
			m_tokoState = TokoState::PUNCH_LEFT;
		}
		if (m_input.IsPress(DIK_S))
		{
			m_tokoState = TokoState::CROUCH_LEFT;
		}
		if (m_input.IsPress(DIK_SPACE))
		{
			m_tokoState = TokoState::JUMP_LEFT;
		}
		break;
	case PlayerScript::JUMP:
		animLoopFlag = false;
		if (animFinished)
		{
			animFinished = false;
			m_tokoState = TokoState::IDLE;
		}
		break;
	case PlayerScript::JUMP_LEFT:
		animLoopFlag = false;
		if (animFinished)
		{
			animFinished = false;
			m_tokoState = TokoState::IDLE_LEFT;
		}
		break;
	case PlayerScript::LOOKAROUND:
		break;
	case PlayerScript::LOOKAROUND_LEFT:
		break;
	case PlayerScript::NEGATIVE:
		break;
	case PlayerScript::NEGATIVE_LEFT:
		break;
	case PlayerScript::POINTAT:
		break;
	case PlayerScript::POINTAT_LEFT:
		break;
	case PlayerScript::POSITIVE:
		break;
	case PlayerScript::POSITIVE_LEFT:
		break;
	case PlayerScript::PUNCH:
		animLoopFlag = false;
		if (animFinished)
		{
			animFinished = false;
			m_tokoState = TokoState::IDLE;
		}
		break;
	case PlayerScript::PUNCH_LEFT:
		animLoopFlag = false;
		if (animFinished)
		{
			animFinished = false;
			m_tokoState = TokoState::IDLE_LEFT;
		}
		break;
	case PlayerScript::RUN:
		animLoopFlag = true;
		if (m_input.IsPress(DIK_S))
		{
			m_tokoState = TokoState::CROUCH;
		}
		if (!(m_input.IsPress(DIK_L)&& m_input.IsPress(DIK_D)))
		{
			m_tokoState = TokoState::IDLE;
		}
		if (m_input.IsPress(DIK_SPACE))
		{
			m_tokoState = TokoState::JUMP;
		}
		if (m_input.IsTrigger(DIK_J))
		{
			m_tokoState = TokoState::ATTACK;
		}
		if (m_input.IsTrigger(DIK_K))
		{
			m_tokoState = TokoState::PUNCH;
		}
		break;
	case PlayerScript::RUN_LEFT:
		animLoopFlag = true;
		if (m_input.IsPress(DIK_S))
		{
			m_tokoState = TokoState::CROUCH_LEFT;
		}
		if (!(m_input.IsPress(DIK_L) && m_input.IsPress(DIK_A)))
		{
			m_tokoState = TokoState::IDLE_LEFT;
		}
		if (m_input.IsPress(DIK_SPACE))
		{
			m_tokoState = TokoState::JUMP_LEFT;
		}
		if (m_input.IsTrigger(DIK_J))
		{
			m_tokoState = TokoState::ATTACK_LEFT;
		}
		if (m_input.IsTrigger(DIK_K))
		{
			m_tokoState = TokoState::PUNCH_LEFT;
		}
		break;
	case PlayerScript::TURN:
		break;
	case PlayerScript::TURN_LEFT:
		break;
	case PlayerScript::WALK:
		animLoopFlag = true;
		if (m_input.IsPress(DIK_S))
		{
			m_tokoState = TokoState::CROUCH;
		}
		if (m_input.IsPress(DIK_L))
		{
			m_tokoState = TokoState::RUN;
		}
		if (!m_input.IsPress(DIK_D))
		{
			if (m_input.IsPress(DIK_A))
			{
				m_tokoState = TokoState::WALK_LEFT;
			}
			m_tokoState = TokoState::IDLE;
		}
		if (m_input.IsPress(DIK_SPACE))
		{
			m_tokoState = TokoState::JUMP;
		}
		if (m_input.IsTrigger(DIK_J))
		{
			m_tokoState = TokoState::ATTACK;
		}
		if (m_input.IsTrigger(DIK_K))
		{
			m_tokoState = TokoState::PUNCH;
		}

		break;
	case PlayerScript::WALK_LEFT:
		animLoopFlag = true;
		if (m_input.IsPress(DIK_S))
		{
			m_tokoState = TokoState::CROUCH_LEFT;
		}
		if (m_input.IsPress(DIK_L))
		{
			m_tokoState = TokoState::RUN_LEFT;
		}
		if (!m_input.IsPress(DIK_A))
		{
			if (m_input.IsPress(DIK_D))
			{
				m_tokoState = TokoState::WALK;
			}
			m_tokoState = TokoState::IDLE_LEFT;
		}
		if (m_input.IsPress(DIK_SPACE))
		{
			m_tokoState = TokoState::JUMP_LEFT;
		}
		if (m_input.IsTrigger(DIK_J))
		{
			m_tokoState = TokoState::ATTACK_LEFT;
		}
		if (m_input.IsTrigger(DIK_K))
		{
			m_tokoState = TokoState::PUNCH_LEFT;
		}

		break;
	case PlayerScript::WARMING_UP:
		break;
	case PlayerScript::WARMING_UP_LEFT:
		break;
	case PlayerScript::WAVE_HAND:
		break;
	case PlayerScript::WAVE_HAND_LEFT:
		break;
	default:
		break;
	}

	animFinished = m_sprite->SetAnimationFrame(deltaTime, m_tokoState, animLoopFlag, animStayLast);
}

void PlayerScript::Draw()
{
	m_sprite->DrawSprite();
}
