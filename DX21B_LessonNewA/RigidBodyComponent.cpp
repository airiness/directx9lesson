#include "RigidBodyComponent.h"

RigidBodyComponent::RigidBodyComponent(PhysicsWorld * physicworld)
	:m_PhysicWorld(*physicworld),m_body(nullptr)
{

}

void RigidBodyComponent::Init()
{
	m_transform = &entity->GetComponent<TransformComponent>();
	m_shape = new CircleShape(0.0f);
	m_body = m_PhysicWorld.CreateBody(m_shape, m_transform->GetPosition());
}

void RigidBodyComponent::Update(const float & deltaTime)
{

}
