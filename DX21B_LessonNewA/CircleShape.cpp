#include "PhysicsHead.h"


CircleShape::CircleShape(float32 r)
{
	m_radius = r;
}

Shape * CircleShape::Clone() const
{
	return new CircleShape(m_radius);
}

void CircleShape::Initialize()
{
	ComputeMass(1.f);
}

void CircleShape::ComputeMass(float32 density)
{
	m_body->m_mass = D3DX_PI * m_radius * m_radius * density;
	m_body->m_imass = (m_body->m_mass) ? 1.f / m_body->m_mass : 0.f;
	m_body->m_inertia = m_body->m_mass * m_radius * m_radius;
	m_body->m_iinertia = (m_body->m_inertia) ? 1.f / m_body->m_inertia : 0.f;
}

void CircleShape::SetOrient(float32 radians)
{
}


void CircleShape::Draw(D3DX21 * device) const
{
	if (m_shouldDraw)
	{
		//draw the circle
		auto lineDev = device->GetLineDevive();
		const uint32 k_segments = 30;
		float32 theta = m_body->m_orient;
		float32 inc = D3DX_PI * 2.f / float32(k_segments);
		Vec2 v[k_segments + 1];

		for (uint32 i = 0; i < k_segments; ++i)
		{
			theta += inc;
			v[i] = { m_radius * Vec2{std::cos(theta),std::sin(theta)} };
			v[i].x += m_body->m_position.x;
			v[i].y += m_body->m_position.y;
		}
		v[k_segments] = v[0];

		lineDev->SetWidth(3.f);
		lineDev->SetAntialias(TRUE);
		lineDev->Draw(v, k_segments+1, m_body->m_color);

		//draw orient
		//VERTEX2D vl[2];
		//Vec2 r(0.f, 1.f);
		//float32 c = (std::cos)(m_body->m_orient);
		//float32 s = (std::sin)(m_body->m_orient);
		//r = { r.x * c - r.y * s, r.x * s + r.y * c };
		//r *= m_radius;
		//r = r + m_body->m_position;
		//vl[0] = { Vec4(m_body->m_position.x,m_body->m_position.y,0.f,0.f) , m_body->m_color };
		//vl[1] = { Vec4(r.x,r.y,0.f,0.f) , m_body->m_color };

		//dev->DrawPrimitiveUP(D3DPT_LINESTRIP, 1, vl, sizeof(VERTEX2D));
	}

}

Shape::Type CircleShape::GetType() const
{
	return e_circle;
}
