#include "PhysicsHead.h"

PhysicsWorld::PhysicsWorld()
{
}


PhysicsWorld::~PhysicsWorld()
{
}

void PhysicsWorld::Initialize(float32 dt, uint32 iter)
{
	m_dt = dt;
	m_iterations = iter;
}

void IntegrateForces(Body* b, float32 dt)
{
	if (b->m_imass == 0.0f)
		return;

	b->m_velocity += (b->m_force * b->m_imass + gravity) * (dt / 2.0f);
	b->m_angularVelocity += b->m_torque * b->m_iinertia * (dt / 2.0f);
}

void IntegrateVelocity(Body* b, float32 dt)
{
	if (b->m_imass == 0.0f)
		return;

	b->m_position += b->m_velocity * dt;
	b->m_orient += b->m_angularVelocity * dt;
	b->SetOrient(b->m_orient);
	IntegrateForces(b, dt);
}


void PhysicsWorld::Step()
{
	// Generate new collision info
	m_contacts.clear();
	for (uint32 i = 0; i < m_bodies.size(); ++i)
	{
		Body* A = m_bodies[i];

		for (uint32 j = i + 1; j < m_bodies.size(); ++j)
		{
			Body* B = m_bodies[j];
			if (A->m_imass == 0 && B->m_imass == 0)
				continue;
			Manifold m(A, B);
			m.Solve();
			if (m.contact_count)
				m_contacts.emplace_back(m);
		}
	}

	// Integrate forces
	for (uint32 i = 0; i < m_bodies.size(); ++i)
		IntegrateForces(m_bodies[i], m_dt);

	// Initialize collision
	for (uint32 i = 0; i < m_contacts.size(); ++i)
		m_contacts[i].Initialize();

	// Solve collisions
	for (uint32 j = 0; j < m_iterations; ++j)
		for (uint32 i = 0; i < m_contacts.size(); ++i)
			m_contacts[i].ApplyImpulse();

	// Integrate velocities
	for (uint32 i = 0; i < m_bodies.size(); ++i)
		IntegrateVelocity(m_bodies[i], m_dt);

	// Correct positions
	for (uint32 i = 0; i < m_contacts.size(); ++i)
		m_contacts[i].PositionalCorrection();

	// Clear all forces
	for (uint32 i = 0; i < m_bodies.size(); ++i)
	{
		Body* b = m_bodies[i];
		b->m_force = { 0, 0 };
		b->m_torque = 0;
	}
}

void PhysicsWorld::DrawShape(D3DX21* device)
{
	for (uint32 i = 0; i < m_bodies.size(); ++i)
	{
		Body* b = m_bodies[i];
		b->m_shape->Draw(device);
	}
}

Body * PhysicsWorld::CreateBody(Shape * shape, Vec2 & position)
{
	assert(shape);
	Body* b = new Body(shape, position);
	m_bodies.push_back(b);
	return b;
}

void PhysicsWorld::SetGravity(float gravity)
{
}

void PhysicsWorld::DestoryBody(Body * body)
{
}

