#pragma once

#ifndef WIN32_LEAN_AND_MEAN
#define WIN32_LEAN_AND_MEAN
#endif

//input process lib
#define DIRECTINPUT_VERSION (0x0800)
#include <dinput.h>
#pragma comment(lib,"dinput8.lib")
#pragma comment(lib,"dxguid.lib")

#include <windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <math.h>
#include <string>
#include <memory>
#include <functional>
#include <initializer_list>
#include <map>
#include <cassert>	//aseert

#ifndef NOMINMAX
#define NOMINMAX
#endif // !NOMINMAX
#include "PIMath.h" //include <algorithm> std::min std::max
#include "DataStruct.h"
// #pragma comment(lib,"d3d9.lib");
// #if defined(DEBUG) || defined(_DEBUG)
// #pragma comment(lib,"d3dx9d.lib")
// #else
// #pragma comment(lib,"d3dx9.lib")
// #endif

#include "RandGenerator.h"

#include <vector>
#include <exception>

inline void ThrowIfFailed(HRESULT hr)
{
	if (FAILED(hr))
	{
		throw std::exception();
	}
}

