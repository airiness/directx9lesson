#include "BulletScript.h"

void BulletScript::Init()
{

	m_transform = &entity->GetComponent<TransformComponent>();
	m_sprite = &entity->GetComponent<SpriteComponent>();
	m_physics = &entity->GetComponent<RigidBodyComponent>();

}

void BulletScript::Update(const float & deltaTime)
{
}

void BulletScript::Draw()
{
}
