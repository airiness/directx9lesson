#pragma once
#include "Body.h"
#include "D3DX21.h"

class Shape
{
public:
	enum Type
	{
		e_circle = 0,
		e_polygon = 1,
		e_typeCount = 2
	};

	Shape() {}
	virtual Shape* Clone() const = 0;
	virtual void Initialize() = 0;
	virtual void ComputeMass(float32 density) = 0;
	virtual void SetOrient(float32 radians) = 0;
	virtual void Draw(D3DX21* device) const = 0;
	virtual Type GetType() const = 0;
	void SetDrawFlag(bool drawFlag);

	Body* m_body;

	float32 m_radius;
	Matrix22 m_u;
	bool m_shouldDraw = true;
};

inline void Shape::SetDrawFlag(bool drawFlag)
{
	m_shouldDraw = drawFlag;
}
