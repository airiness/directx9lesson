#pragma once
#include "pch.h"

class Input
{
public:
	Input();
	~Input();

	bool Initialize(HINSTANCE, HWND, int, int);
	void Shutdown();
	bool Update();

	bool IsPress(int);
	bool IsTrigger(int);
	bool IsRelease(int);

	void GetMouseLocation(int& mouseX, int& mouseY);

private:
	bool ReadKeyboard();
	bool ReadMouse();
	void ProcessInput();
	bool InitializeDirectInput(HINSTANCE);

	LPDIRECTINPUT8 m_pInput;
	LPDIRECTINPUTDEVICE8 m_pDevKeyboard;
	LPDIRECTINPUTDEVICE8 m_pDevMouse;

	BYTE m_aKeyState[256];
	BYTE m_aKeyStateTrigger[256];
	BYTE m_aKeyStateRelease[256];

	DIMOUSESTATE m_aMouseState;

	int m_screenWidth, m_screenHeight;
	int m_mouseX, m_mouseY;
};