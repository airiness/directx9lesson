#pragma once

class Body;

class Manifold
{
public:
	Manifold(Body* a, Body* b);

	void Solve();                 // Generate contact information
	void Initialize();            // Precalculations for impulse solving
	void ApplyImpulse();          // Solve impulse and apply
	void PositionalCorrection();  // Naive correction of positional penetration
	void InfiniteMassCorrection();

	Body* A;
	Body* B;

	float32 penetration;  // Depth of penetration from collision
	Vec2 normal;          // From A to B
	Vec2 contacts[2];     // Points of contact during collision
	uint32 contact_count; // Number of contacts that occured during collision
	float32 e;            // Mixed restitution
	float32 df;           // Mixed dynamic friction
	float32 sf;           // Mixed static friction
};

