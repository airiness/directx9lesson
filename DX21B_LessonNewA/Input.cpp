#include "Input.h"

Input::Input()
	:m_pInput(nullptr),
	m_pDevKeyboard(nullptr),
	m_pDevMouse(nullptr)
{
}

Input::~Input()
{
}

bool Input::ReadKeyboard()
{
	HRESULT result;
	BYTE akeyState[256];

	// Read the keyboard device.
	result = m_pDevKeyboard->GetDeviceState(sizeof(akeyState), (LPVOID)&akeyState);
	if (SUCCEEDED(result))
	{
		for (int nCnKey = 0; nCnKey < 256; nCnKey++)
		{
			// キートリガー・リリース情報を生成
			m_aKeyStateTrigger[nCnKey] = (m_aKeyState[nCnKey] ^ akeyState[nCnKey]) & akeyState[nCnKey];
			m_aKeyStateRelease[nCnKey] = (m_aKeyState[nCnKey] ^ akeyState[nCnKey]) & m_aKeyState[nCnKey];


			// キープレス情報を保存
			m_aKeyState[nCnKey] = akeyState[nCnKey];
		}
	}
	else
	{
		// If the keyboard lost focus or was not acquired then try to get control back.
		if ((result == DIERR_INPUTLOST) || (result == DIERR_NOTACQUIRED))
		{
			m_pDevKeyboard->Acquire();
		}
		else
		{
			return false;
		}
	}

	return true;
}

bool Input::ReadMouse()
{
	HRESULT result;

	// Read the mouse device.
	result = m_pDevMouse->GetDeviceState(sizeof(DIMOUSESTATE), (LPVOID)&m_aMouseState);
	if (FAILED(result))
	{
		// If the mouse lost focus or was not acquired then try to get control back.
		if ((result == DIERR_INPUTLOST) || (result == DIERR_NOTACQUIRED))
		{
			m_pDevMouse->Acquire();
		}
		else
		{
			return false;
		}
	}

	return true;
}

void Input::ProcessInput()
{
	// Update the location of the mouse cursor based on the change of the mouse location during the frame.
	m_mouseX += m_aMouseState.lX;
	m_mouseY += m_aMouseState.lY;

	// Ensure the mouse location doesn't exceed the screen width or height.
	if (m_mouseX < 0) { m_mouseX = 0; }
	if (m_mouseY < 0) { m_mouseY = 0; }

	if (m_mouseX > m_screenWidth) { m_mouseX = m_screenWidth; }
	if (m_mouseY > m_screenHeight) { m_mouseY = m_screenHeight; }

	return;
}

bool Input::InitializeDirectInput(HINSTANCE hInstance)
{
	if (!m_pInput)
	{
		if (FAILED(DirectInput8Create(hInstance, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&m_pInput, nullptr))) {

			return false;
		}
	}
	return true;
}

bool Input::Initialize(HINSTANCE hInstance, HWND hWnd, int screenWidth, int screenHeight)
{
	m_screenWidth = screenWidth;
	m_screenHeight = screenHeight;

	m_mouseX = m_mouseY = 0;

	// 入力処理の初期化
	if (!InitializeDirectInput(hInstance)) {

		MessageBox(hWnd, "DirectInputオブジェクトが作れねぇ！", "警告！", MB_ICONWARNING);
		return false;
	}

	// デバイスの作成
	if (FAILED(m_pInput->CreateDevice(GUID_SysKeyboard, &m_pDevKeyboard, NULL)))
	{
		MessageBox(hWnd, "キーボードがねぇ！", "警告！", MB_ICONWARNING);
		return false;
	}

	// データフォーマットを設定
	if (FAILED(m_pDevKeyboard->SetDataFormat(&c_dfDIKeyboard)))
	{
		MessageBox(hWnd, "キーボードのデータフォーマットを設定できませんでした。", "警告！", MB_ICONWARNING);
		return false;
	}

	// 協調モードを設定（フォアグラウンド＆非排他モード）
	if (FAILED(m_pDevKeyboard->SetCooperativeLevel(hWnd, (DISCL_FOREGROUND | DISCL_NONEXCLUSIVE))))
	{
		MessageBox(hWnd, "キーボードの協調モードを設定できませんでした。", "警告！", MB_ICONWARNING);
		return false;
	}

	// キーボードへのアクセス権を獲得(入力制御開始)
	m_pDevKeyboard->Acquire();

	if (FAILED(m_pInput->CreateDevice(GUID_SysMouse, &m_pDevMouse, nullptr)))
	{
		MessageBox(hWnd, "mouseがねぇぇぇぇｘ！", "警告！", MB_ICONWARNING);
		return false;
	}

	if (FAILED(m_pDevMouse->SetDataFormat(&c_dfDIMouse)))
	{
		MessageBox(hWnd, "マウスのデータフォーマットを設定できねぇぇぇぇ", "警告！", MB_ICONWARNING);
		return false;
	}

	if (FAILED(m_pDevMouse->SetCooperativeLevel(hWnd, DISCL_FOREGROUND | DISCL_NONEXCLUSIVE)))
	{
		MessageBox(hWnd, "マウス協調モードを設定できねぇぇぇぇ", "警告！", MB_ICONWARNING);
		return false;
	}

	m_pDevMouse->Acquire();

	return true;
}

void Input::Shutdown()
{
	if (m_pDevMouse)
	{
		m_pDevMouse->Unacquire();
		m_pDevMouse->Release();
		m_pDevMouse = nullptr;
	}

	if (m_pDevKeyboard)
	{
		m_pDevKeyboard->Unacquire();
		m_pDevKeyboard->Release();
		m_pDevKeyboard = nullptr;
	}

	if (m_pInput)
	{
		m_pInput->Release();
		m_pInput = nullptr;
	}
}

bool Input::Update()
{
	bool result;
	// Read the current state of the keyboard.
	result = ReadKeyboard();
	if (!result)
	{
		return false;
	}

	// Read the current state of the mouse.
	result = ReadMouse();
	if (!result)
	{
		return false;
	}
	// Process the changes in the mouse and keyboard.
	ProcessInput();
	return true;
}

bool Input::IsPress(int nKey)
{
	return (m_aKeyState[nKey] & 0x80) ? true : false;
}

bool Input::IsTrigger(int nKey)
{
	return (m_aKeyStateTrigger[nKey] & 0x80) ? true : false;
}

bool Input::IsRelease(int nKey)
{
	return (m_aKeyStateRelease[nKey] & 0x80) ? true : false;
}

void Input::GetMouseLocation(int & mouseX, int & mouseY)
{
	mouseX = m_mouseX;
	mouseY = m_mouseY;
}
