#include "PhysicsHead.h"

Body::Body(Shape * shape, Vec2 & pos)
	:m_shape(shape->Clone()),m_position(pos)
{
	m_shape->m_body = this;
	m_velocity = { 0,0 };
	m_angularVelocity = 0;
	m_torque = 0;
	m_orient = Randr(-D3DX_PI, D3DX_PI);
	m_force = { 0, 0 };
	m_staticFriction = 0.5f;
	m_dynamicFriction = 0.3f;
	m_restitution = 0.8f;
	m_shape->Initialize();
	m_color = D3DCOLOR_RGBA(Randi(0, 255), Randi(0, 255), Randi(0, 255), 255);
}

Body::~Body()
{
}

void Body::ApplyForce(const Vec2 & f)
{
	m_force += f;
}

void Body::ApplyImpulse(const Vec2 & impulse, const Vec2 & contactVector)
{
	m_velocity += m_imass * impulse;
	m_angularVelocity += m_iinertia * Cross(contactVector, impulse);
}

void Body::SetStatic()
{
	m_inertia = 0.0f;
	m_iinertia = 0.0f;
	m_mass = 0.0f;
	m_imass = 0.0f;
}

void Body::SetOrient(float32 radians)
{
	m_orient = radians;
	m_shape->SetOrient(radians);
}
