/*2019.6
 *author: zhang ruisong
 *texture manager class
 */
#pragma once
#include "D3DX21.h"

class TextureManager
{
private:
	using TexturePointer = LPDIRECT3DTEXTURE9;
public:
	TextureManager(D3DX21* device);
	~TextureManager();

	void Initialize();
	void Shutdown();

	bool GetTexture(const std::string& texName, LPDIRECT3DTEXTURE9& texture);

	void ClearAll();
	void UnLoad(const std::string& texName);

	bool GetTextureSize(const std::string& texName, Vec2& texSize);
private:
	void ReleaseTexture(LPDIRECT3DTEXTURE9* texture);
	bool LoadTexturesFromFile(const std::string& filename);
private:
	D3DX21* m_pDevice;
	std::unique_ptr<std::map<std::string, LPDIRECT3DTEXTURE9>> m_pTextures;
};