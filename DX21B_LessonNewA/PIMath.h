/*2019.7.13
 *author : zhang ruisong
 *simple math head include 2x2matrix
 */

#pragma once
#include <algorithm>
#include <d3dx9math.h>
#include <random>

#define Len(x) D3DXVec2Length(x)
#define LenSq(x) D3DXVec2LengthSq(x)
#define MatrixIdentity(x) D3DXMatrixIdentity(x)
 //define 
using Vec2 = D3DXVECTOR2;
using Vec3 = D3DXVECTOR3;
using Vec4 = D3DXVECTOR4;
using Matrix = D3DXMATRIX;
using Color = D3DCOLOR;
using int8 = signed char;
using int16 = signed short;
using int32 = signed int;
using uint8 = unsigned char;
using uint16 = unsigned short;
using uint32 = unsigned int;
using float32 = float;
using float64 = double;

//only have in windows
#ifdef WIN32
using int64 = __int64;
using uint64 = unsigned __int64;
#endif // WIN32

const float32 EPSILON = 0.00001f;
const float dt = 1.0f / 120.0f;

struct Matrix22
{
	union
	{
		struct
		{
			float32 m00, m01;
			float32 m10, m11;
		};

		float32 m[2][2];
		float32 v[4];
	};

	Matrix22() {}
	Matrix22(float32 radians)
	{
		float32 c = std::cos(radians);
		float32 s = std::sin(radians);

		m00 = c; m01 = -s;
		m10 = s; m11 = c;
	}

	Matrix22(float32 a, float32 b, float32 c, float32 d)
		: m00(a), m01(b)
		, m10(c), m11(d)
	{
	}

	void Set(float32 radians)
	{
		float32 c = std::cos(radians);
		float32 s = std::sin(radians);

		m00 = c; m01 = -s;
		m10 = s; m11 = c;
	}

	Matrix22 Abs(void) const
	{
		return Matrix22(std::abs(m00), std::abs(m01), std::abs(m10), std::abs(m11));
	}

	Vec2 AxisX(void) const
	{
		return Vec2(m00, m10);
	}

	Vec2 AxisY(void) const
	{
		return Vec2(m01, m11);
	}

	Matrix22 Transpose(void) const
	{
		return Matrix22(m00, m10, m01, m11);
	}

	const Vec2 operator*(const Vec2& rhs) const
	{
		return Vec2(m00 * rhs.x + m01 * rhs.y, m10 * rhs.x + m11 * rhs.y);
	}

	const Matrix22 operator*(const Matrix22& rhs) const
	{
		// [00 01]  [00 01]
		// [10 11]  [10 11]

		return Matrix22(
			m[0][0] * rhs.m[0][0] + m[0][1] * rhs.m[1][0],
			m[0][0] * rhs.m[0][1] + m[0][1] * rhs.m[1][1],
			m[1][0] * rhs.m[0][0] + m[1][1] * rhs.m[1][0],
			m[1][0] * rhs.m[0][1] + m[1][1] * rhs.m[1][1]
		);
	}

};

struct Rot
{
	float32 s, c;

	Rot() {}

	explicit Rot(float32 angle)
	{
		s = sinf(angle);
		c = cosf(angle);
	}

	void Set(float32 angle)
	{
		s = sinf(angle);
		c = cosf(angle);
	}

	void SetIdentity()
	{
		s = 0.f;
		c = 1.f;
	}

	float32 GetAngle() const
	{
		return atan2f(s, c);
	}

	Vec2 GetXAxis() const
	{
		return Vec2(c, s);
	}

	Vec2 GetYAxis() const
	{
		return Vec2(-s, c);
	}

};


struct Transform
{
	Vec2 p;
	Rot r;
	Transform() {}
	Transform(const Vec2& position, const Rot& rotation) : p(position), r(rotation) {}

	void SetIdentity()
	{
		p.x = 0.f;
		p.y = 0.f;
		r.SetIdentity();
	}

	void Set(const Vec2& position, float32 angle)
	{
		p = position;
		r.Set(angle);
	}
};


inline void Vec2Normalize(Vec2& vec2)
{
	float32 len = Len(&vec2);
	if (len > EPSILON)
	{
		float32 invLen = 1.0f / len;
		vec2.x *= invLen;
		vec2.y *= invLen;
	}
}

inline float32 Dot(const Vec2& a, const Vec2& b)
{
	return a.x * b.x + a.y * b.y;
}

inline float32 Sqr(float32 a)
{
	return a * a;
}

inline float32 DistSqr(const Vec2& a, const Vec2& b)
{
	Vec2 c = a - b;
	return Dot(c, c);
}

inline bool BiasGreaterThan(float32 a, float32 b)
{
	const float32 k_biasRelative = 0.95f;
	const float32 k_biasAbsolute = 0.01f;
	return a >= b * k_biasRelative + a * k_biasAbsolute;
}

inline Vec2 Cross(const Vec2& v, float32 a)
{
	return Vec2(a * v.y, -a * v.x);
}

inline Vec2 Cross(float32 a, const Vec2& v)
{
	return Vec2(-a * v.y, a * v.x);
}

inline float32 Cross(const Vec2& a, const Vec2& b)
{
	return a.x * b.y - a.y * b.x;
}

// Comparison with tolerance of EPSILON
inline bool Equal(float32 a, float32 b)
{
	// <= instead of < for NaN comparison safety
	return std::abs(a - b) <= EPSILON;
}

