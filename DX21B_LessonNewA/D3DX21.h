#pragma once
#include "pch.h"


class D3DX21
{
public:
	D3DX21();
	D3DX21(const D3DX21&) = delete;
	~D3DX21();

	bool Initialize(int, int, HWND);
	void Shutdown();

	void BeginScene(int, int, int, int);
	void EndScene();

	LPDIRECT3DDEVICE9 GetDevice() const;
	LPD3DXLINE GetLineDevive() const;

private:
	LPDIRECT3D9 m_pD3D;
	LPDIRECT3DDEVICE9 m_pDevice;
	LPD3DXLINE m_pLine;
};

