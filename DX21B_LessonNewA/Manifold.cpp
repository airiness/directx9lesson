#include "PhysicsHead.h"

Manifold::Manifold(Body * a, Body * b)
	:A(a),B(b)
{
}

void Manifold::Solve()
{
	Dispatch[A->m_shape->GetType()][B->m_shape->GetType()](this, A, B);
}

void Manifold::Initialize()
{
	// Calculate average restitution
	e = (std::min)(A->m_restitution, B->m_restitution);

	// Calculate static and dynamic friction
	sf = std::sqrt(A->m_staticFriction * B->m_staticFriction);
	df = std::sqrt(A->m_dynamicFriction * B->m_dynamicFriction);

	for (uint32 i = 0; i < contact_count; ++i)
	{
		// Calculate radii from COM to contact
		Vec2 ra = contacts[i] - A->m_position;
		Vec2 rb = contacts[i] - B->m_position;

		Vec2 rv = B->m_velocity + Cross(B->m_angularVelocity, rb) -
			A->m_velocity - Cross(A->m_angularVelocity, ra);


		// Determine if we should perform a resting collision or not
		// The idea is if the only thing moving this object is gravity,
		// then the collision should be performed without any restitution
		if (LenSq(&rv) < LenSq(&(dt * gravity)) + EPSILON)
			e = 0.0f;
	}
}

void Manifold::ApplyImpulse()
{
	// Early out and positional correct if both objects have infinite mass
	if (Equal(A->m_imass + B->m_imass, 0))
	{
		InfiniteMassCorrection();
		return;
	}

	for (uint32 i = 0; i < contact_count; ++i)
	{
		// Calculate radii from COM to contact
		Vec2 ra = contacts[i] - A->m_position;
		Vec2 rb = contacts[i] - B->m_position;

		// Relative velocity
		Vec2 rv = B->m_velocity + Cross(B->m_angularVelocity, rb) -
				  A->m_velocity - Cross(A->m_angularVelocity, ra);

		// Relative velocity along the normal
		float32 contactVel = Dot(rv, normal);

		// Do not resolve if velocities are separating
		if (contactVel > 0)
			return;

		float32 raCrossN = Cross(ra, normal);
		float32 rbCrossN = Cross(rb, normal);
		float32 invMassSum = A->m_imass + B->m_imass + Sqr(raCrossN) * A->m_iinertia + Sqr(rbCrossN) * B->m_iinertia;

		// Calculate impulse scalar
		float32 j = -(1.0f + e) * contactVel;
		j /= invMassSum;
		j /= float32(contact_count);

		// Apply impulse
		Vec2 impulse = normal * j;
		A->ApplyImpulse(-impulse, ra);
		B->ApplyImpulse(impulse, rb);

		// Friction impulse
		rv = B->m_velocity + Cross(B->m_angularVelocity, rb) -
			 A->m_velocity - Cross(A->m_angularVelocity, ra);

		Vec2 t = rv - (normal * Dot(rv, normal));
		Vec2Normalize(t);

		// j tangent magnitude
		float32 jt = -Dot(rv, t);
		jt /= invMassSum;
		jt /= float32(contact_count);

		// Don't apply tiny friction impulses
		if (Equal(jt, 0.0f))
			return;

		// Coulumb's law
		Vec2 tangentImpulse;
		if (std::abs(jt) < j * sf)
			tangentImpulse = t * jt;
		else
			tangentImpulse = t * -j * df;

		// Apply friction impulse
		A->ApplyImpulse(-tangentImpulse, ra);
		B->ApplyImpulse(tangentImpulse, rb);
	}
}

void Manifold::PositionalCorrection()
{
	const float32 k_slop = 0.05f; // Penetration allowance
	const float32 percent = 0.4f; // Penetration percentage to correct
	Vec2 correction = ((std::max)(penetration - k_slop, 0.0f) / (A->m_imass + B->m_imass)) * normal * percent;
	A->m_position -= correction * A->m_imass;
	B->m_position += correction * B->m_imass;
}

void Manifold::InfiniteMassCorrection()
{
	A->m_velocity = { 0, 0 };
	B->m_velocity = { 0, 0 };
}
