#pragma once
#include "PhysicsHead.h"
#include "TransformComponent.h"

class RigidBodyComponent : public Component
{
private:
	enum RigidBodyType
	{
		RigidBody_Block,
		RigidBody_Sphere,
		RigidBody_Polygon
	};
private:
	Shape* m_shape;
	Body* m_body;
	PhysicsWorld& m_PhysicWorld;
	TransformComponent* m_transform;

public:
	RigidBodyComponent(PhysicsWorld* physicworld);

	void Init() override;
	void Update(const float& deltaTime) override;

	//void SetRigidBody(RigidBodyType type,);
};
