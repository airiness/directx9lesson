//////////////////////////////////////////////////////////////////////////
//date:9012.5.8
//author:zhang ruisong
//in lesson
//
//////////////////////////////////////////////////////////////////////////
#include <iostream>
#include "System.h"

int main(int argc,char* argv[])
{
	bool result;

	std::cout << "DX21 home work" <<std::endl;
	std::cout << "By zhang ruisong" <<std::endl;

	std::unique_ptr<System> BaseSystem = std::make_unique<System>();
	if (!BaseSystem)
	{
		return -1;
	}

	result = BaseSystem->Initialize();
	if (result)
	{
		BaseSystem->Run();
	}

	BaseSystem->Shutdown();
	BaseSystem.reset();
	BaseSystem = nullptr;

	return 0;
}
