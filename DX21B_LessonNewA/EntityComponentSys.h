#pragma once
#include <bitset>
#include <vector>
#include <memory>
#include <algorithm>
#include <array>
#include <cassert>
#include <type_traits>

class Component;
class Entity;

using ComponentID = std::size_t;


inline ComponentID GetUniqueComponentID() noexcept
{
	static ComponentID lastID{ 0u };
	return lastID++;
}


template<typename T>
inline ComponentID GetComponentTypeID() noexcept
{
	static_assert(std::is_base_of<Component, T>::value,
		"T must inherit from Component");

	static ComponentID typeID{ GetUniqueComponentID() };
	return typeID;
}

constexpr std::size_t maxComponents{ 32 };
using ComponentBitset = std::bitset<maxComponents>;
using ComponentArray = std::array<Component*, maxComponents>;

class Component
{
public:
	Entity* entity;

	virtual void Init() {}
	virtual void Update(const float& deltaTime) {}
	virtual void Draw() {}

	virtual ~Component() {}
};

class Entity
{
private:
	bool active{ true };
	std::vector<std::unique_ptr<Component>> components;

	ComponentArray componentArray;
	ComponentBitset componentBitset;
public:
	void  Update(float deltaTime)
	{
		for (auto& c : components) { c->Update(deltaTime); }
	}

	void Draw()
	{
		for (auto& c : components) { c->Draw(); }
	}

	bool IsActive() const { return active; }
	void Destory() { active = false; }

	template<typename T>
	bool HasComponent() const
	{
		return componentBitset[GetComponentTypeID<T>()];
	}

	template <typename T, typename... TArgs>
	T& AddComponent(TArgs&&... mArgs)
	{
		assert(!HasComponent<T>());

		T* c(new T(std::forward<TArgs>(mArgs)...));
		c->entity = this;
		std::unique_ptr<Component> uPtr{ c };
		components.emplace_back(std::move(uPtr));

		componentArray[GetComponentTypeID<T>()] = c;
		componentBitset[GetComponentTypeID<T>()] = true;

		c->Init();

		return *c;
	}

	template<typename T>
	T& GetComponent() const
	{
		assert(HasComponent<T>());
		auto ptr(componentArray[GetComponentTypeID<T>()]);
		return *static_cast<T*>(ptr);
	}

};

class EntityManager
{
private:
	std::vector<std::unique_ptr<Entity>> entities;

public:
	void Update(const float& deltaTime)
	{
		for (auto & e : entities)
		{
			e->Update(deltaTime);
		}
	}

	void Draw()
	{
		for (auto & e : entities)
		{
			e->Draw();
		}
	}

	void Refresh()
	{
		entities.erase(
			std::remove_if(std::begin(entities), std::end(entities),
				[](const std::unique_ptr<Entity>& mEntity)
		{
			return !mEntity->IsActive();
		}),
			std::end(entities));
	}

	Entity& AddEntity()
	{
		Entity* e{ new Entity{} };
		std::unique_ptr<Entity> uPtr{ e };
		entities.emplace_back(std::move(uPtr));
		return *e;
	}
};