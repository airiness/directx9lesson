#include "TransformComponent.h"

TransformComponent::TransformComponent()
	:m_position(Vec2{ 0.0f,0.0f }), m_scale(Vec2{1.0f,1.0f}),m_rotation(0.0f)
{
}

void TransformComponent::Init()
{
}

Vec2 & TransformComponent::GetPosition()
{
	return m_position;
}

Vec2 & TransformComponent::GetScale()
{
	return m_scale;
}

float TransformComponent::GetRotation()
{
	return m_rotation;
}

void TransformComponent::SetPosition(const Vec2 & position)
{
	m_position = position;
}
