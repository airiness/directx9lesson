#pragma once
#include "Shape.h"

#define MaxPolyVertexCount (16)

class PolygonShape : public Shape
{
public:
	void Initialize() override;
	Shape* Clone() const override;
	void ComputeMass(float32 density) override;
	void SetOrient(float32 radians) override;
	void Draw(D3DX21* device) const override;
	Type GetType() const override;

	void SetBox(float32 width,float32 height);
	void Set(Vec2* vertices, uint32 count);
	Vec2 GetSupport(const Vec2& dir);

	uint32 m_vertexCount;
	Vec2 m_vertices[MaxPolyVertexCount];
	Vec2 m_normals[MaxPolyVertexCount];
};

